package id.phully.discoveryone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class DiscoveryOneApplication {

    public static void main(String[] args) {
        SpringApplication.run(DiscoveryOneApplication.class, args);
    }

}

